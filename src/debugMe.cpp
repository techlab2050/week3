#include <stdlib.h>

enum class Gender
{
    Male,
    Female,
    Other
};

struct Person
{
    int age;
    Gender gender; 
    const char* name;
};

struct WidgetWorkBuffer
{
    char* pData;
};

struct Widget
{
    int numParts;
    bool ready;
};

Person* createPerson(int age, Gender gender, const char* name)
{
    Person person;
    person.age = age;
    person.gender = gender;
    person.name = name;
    return &person; 
}

void doStuffWithWidgets(const Widget* pWidgets, int numWidgets)
{
   WidgetWorkBuffer* pBuffers = new WidgetWorkBuffer[numWidgets];

   for(int i = 0; i < numWidgets; ++i)
   {
       pBuffers[i].pData = new char[1024];

       const Widget* pWidget = &pWidgets[i];

       // Do stuff with pData and pWidget that aren't important to this exercise
   }

   delete pBuffers; 
}

void createAndProcessWidgets()
{
    const int NUM_WIDGETS = 10;
    Widget* pWidgets = new Widget[NUM_WIDGETS];
    for(int i = 0; i <= NUM_WIDGETS; ++i)
    {
        pWidgets[i].numParts = rand() % 50;
        pWidgets[i].ready = false;
    }

    doStuffWithWidgets(pWidgets, NUM_WIDGETS);
}
