#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Player.h"

//-------------------------------------------------
// WRITE THIS FUNCTION PER THE SPEC IN THE SLIDES
//-------------------------------------------------
void memoryExercise()
{
    // Answer here
}


//-------------------------------------------------
// CODE TO TEST MODULES
//-------------------------------------------------
void testPlayerModule()
{
    Player player;
    initPlayer(&player);
    Vec2D pos = getPlayerPosition(&player);
    printf("The player began with name '%s'\n", getPlayerName(&player));
    printf("Its initial score was '%d'\n", getPlayerScore(&player));
    printf("Its initial health was '%d'\n", getPlayerHealth(&player));
    printf("Its initial position was (%.2f,%.2f)\n", pos.x, pos.y);

    printf("Setting name to 'tinto'...\n");
    setPlayerName(&player, "tinto");
    printf("Player name now reports as '%s'\n", getPlayerName(&player));

    printf("Setting score to 123456...\n");
    setPlayerScore(&player, 123456);
    printf("Player score now reports as '%d'\n", getPlayerScore(&player));

    printf("Setting health to 100...\n");
    setPlayerHealth(&player, 100);
    printf("Player health now reports as '%d'\n", getPlayerHealth(&player));

    printf("Setting position to (99,100)...\n");
    setPlayerPosition(&player, 99, 100);
    pos = getPlayerPosition(&player);
    printf("Player position now reports as (%.2f,%.2f)\n", pos.x, pos.y);
}

int main()
{
    testPlayerModule();
}